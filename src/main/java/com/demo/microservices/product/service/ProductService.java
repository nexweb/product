package com.demo.microservices.product.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.microservices.product.dto.ProductDto;
import com.demo.microservices.product.model.Product;
import com.demo.microservices.product.repository.ProductRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProductService {
	private static final Logger logger = LoggerFactory.getLogger(ProductService.class);
	
	@Autowired
	private ProductRepository productRepository;
	
	@Value("${bootcamp.product-max-count}")
	private Long productMaxCount;	
	
	/**
	 * 상품  조회  
	 * @return
	 */
	public List<Product> findAll() {
		return productRepository.findAll(productMaxCount);
	}
	
	/**
	 * 상품 id 조회  
	 * @return
	 */
	public Product findById(Long productId) {
		return productRepository.findById(productId);
	}
	
	/**
	 * 상품 id 조회  
	 * @return
	 */
	public ProductDto findByName(String productCode) {
		Product product = productRepository.findByProductName(productCode);
		
		log.info("findByName :{}", product, productCode);
		ProductDto productDto = new ProductDto();
		productDto.setCategory(product.getCategory());
		productDto.setDescription(product.getDescription());
		productDto.setName(product.getName());
		productDto.setPrice(productDto.getPrice());
		
		return productDto;
	}
	
	/**
	 * 상품 수정 
	 * @param stockDto
	 */
	@Transactional
	public ProductDto updateProduct(ProductDto productDto) {
		Product product = productRepository.findByProductName(productDto.getName());
		ProductDto prodDto = new ProductDto();
		
		if (product != null) {
			
			product.setName(productDto.getName());
			product.setDescription(productDto.getDescription());
			product.setPrice(productDto.getPrice());
			product.setCategory(productDto.getCategory());
			
			log.info("updateProuct:{}", product);
			
			productRepository.updateProduct(product);
			product = productRepository.findByProductName(productDto.getName());
			

			prodDto.setCategory(product.getCategory());
			prodDto.setDescription(product.getDescription());
			prodDto.setName(product.getName());
			prodDto.setPrice(productDto.getPrice());
		}
		
		return prodDto;
	}
	
	/**
	 * 상품 수정 
	 * @param stockDto
	 */
	@Transactional
	public ProductDto saveProduct(ProductDto productDto) {
		Product product = productRepository.findByProductName(productDto.getName());
		ProductDto prodDto = new ProductDto();
		if (product == null) {
			product = new Product();
			
			product.setName(productDto.getName());
			product.setDescription(productDto.getDescription());
			product.setPrice(productDto.getPrice());
			product.setCategory(productDto.getCategory());
			
			productRepository.saveProduct(product);
			
			product = productRepository.findByProductName(productDto.getName());
			
			prodDto.setCategory(product.getCategory());
			prodDto.setDescription(product.getDescription());
			prodDto.setName(product.getName());
			prodDto.setPrice(product.getPrice());
			
		} else {
			throw new RuntimeException("상품이 존재합니다.");
		}
		
		return prodDto;

	}	
	
	
	
	
	/**
	 * 상품 삭제  
	 * @param productDto
	 */
	@Transactional
	public void deleteProduct(String productName) {
		Product product = productRepository.findByProductName(productName);
		logger.info("product id :{}, product: {}", productName, product);
		productRepository.deleteProduct(product.getId());
	}		
}
