package com.demo.microservices.product.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Product {
	private Long id;
	private String name;
	private Long price;
	private String category;
	private String description;
}
