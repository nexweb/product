package com.demo.microservices.product.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.demo.microservices.product.dto.ProductDto;
import com.demo.microservices.product.model.Product;
import com.demo.microservices.product.service.ProductService;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(value = "Product V2")
@RestController
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	@Value("${bootcamp.payment.server}")
	private String paymentServer;
	
	@Value("${bootcamp.payment.port}")
	private String paymentPort;
//
//    @Autowired
//    public void setProductService(ProductService productService) {
//        this.productService = productService;
//    }

//    @Cacheable(value="product", key="#productCode")
    @GetMapping("/products/{productName}")
    public ProductDto findByProductCode(@PathVariable String productName) {
    	ProductDto productDto = productService.findByName(productName);
    	log.info("Products : {}", productDto);
    	
    	return Optional.ofNullable(productDto).orElseThrow(()->new RuntimeException("데이터가 없습니다."));
    	
    }
    
//    @CachePut(value="product", key="#productDto.name")
    @PutMapping("/products")
    public ProductDto updateProduct(@RequestBody ProductDto productDto) {
    	log.info("Products param: {}", productDto);
    	ProductDto prodDto = productService.updateProduct(productDto);
    	log.info("Products : {}", productDto);
    	
    	return prodDto;
    }
    

    @GetMapping("/products")
    public ResponseEntity<List<Product>> getProducts() {
    	log.info("Products ");
    	List<Product> products = productService.findAll();
    	log.info("Products : {}", products);
    	
    	return new ResponseEntity<>(products, HttpStatus.OK);
    }
    
//	@Cacheable(value="product", key="#productDto.name")
    @PostMapping("/products")
    public ProductDto saveProduct(@RequestBody ProductDto productDto) {
    	ProductDto prodDto = productService.saveProduct(productDto);
    	return prodDto;
    }
     
//	@CacheEvict(value="product",key="#productName")
    @DeleteMapping("/products/{productName}")
    public ResponseEntity<String> deleteProduct(@PathVariable String productName) {
    	productService.deleteProduct(productName);
    	
    	return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
    
    @GetMapping("/products/payments")
    public ResponseEntity<String> getPayments() {
		String uri = String.format("http://%s:%s/payments/hello", paymentServer, paymentPort);
		
		String payments = null;

		RestTemplate rest = new RestTemplate();
//		Object products = null;
		
		try {
			log.info("call payments REST API");
			
			payments = rest.exchange(uri, HttpMethod.GET, null, String.class).getBody();
			 
			log.info("call payments REST API :{}", payments);
		} catch (Exception e) {
			log.error("ERROR", e);
			throw new RuntimeException(e);
		}
		log.info("success get payments");
		
		
		return new ResponseEntity<String>(payments, HttpStatus.OK);
    }
}
