package com.demo.microservices.product.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class HelloController {
	@GetMapping("/hello")
	public ResponseEntity<String> hello() {
		String message = "Hello Product";
		log.info(message);
		return new ResponseEntity<String>(message, HttpStatus.OK);
	}
}
